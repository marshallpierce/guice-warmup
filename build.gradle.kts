import java.net.URI
import java.time.Duration

plugins {
    `java-library`
    id("net.researchgate.release") version "2.8.1"
    id("io.github.gradle-nexus.publish-plugin") version "1.1.0"
    id("com.github.ben-manes.versions") version "0.39.0"
    `maven-publish`
    signing
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("com.google.inject:guice:5.0.1")

    testImplementation("org.junit.jupiter:junit-jupiter-api:5.7.1")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.7.1")
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
    withSourcesJar()
    withJavadocJar()
}

group = "org.mpierce.guice.warmup"

tasks {
    test {
        useJUnitPlatform()
    }

    afterReleaseBuild {
        dependsOn(provider { project.tasks.named("publishToSonatype") })
    }
}

publishing {
    publications {
        register<MavenPublication>("sonatype") {
            from(components["java"])
            // sonatype required pom elements
            pom {
                name.set("${project.group}:${project.name}")
                description.set(name)
                url.set("https://bitbucket.org/marshallpierce/guice-warmup")
                licenses {
                    license {
                        name.set("Copyfree Open Innovation License 0.5")
                        url.set("https://copyfree.org/content/standard/licenses/coil/license.txt")
                    }
                }
                developers {
                    developer {
                        id.set("marshallpierce")
                        name.set("Marshall Pierce")
                        email.set("575695+marshallpierce@users.noreply.github.com")
                    }
                }
                scm {
                    connection.set("scm:git:https://bitbucket.org/marshallpierce/guice-warmup")
                    developerConnection.set("scm:git:ssh://git@bitbucket.org:marshallpierce/guice-warmup.git")
                    url.set("https://bitbucket.org/marshallpierce/guice-warmup")
                }
            }
        }
    }

    // A safe throw-away place to publish to:
    // ./gradlew publishSonatypePublicationToLocalDebugRepository -Pversion=foo
    repositories {
        maven {
            name = "localDebug"
            url = URI.create("file:///${project.buildDir}/repos/localDebug")
        }
    }
}

// don't barf for devs without signing set up
if (project.hasProperty("signing.keyId")) {
    configure<SigningExtension> {
        sign(project.extensions.getByType<PublishingExtension>().publications["sonatype"])
    }
}

nexusPublishing {
    repositories {
        sonatype {
            // sonatypeUsername and sonatypePassword properties are used automatically
            stagingProfileId.set("ab8c5618978d18") // org.mpierce
        }
    }
    // these are not strictly required. The default timeouts are set to 1 minute. But Sonatype can be really slow.
    // If you get the error "java.net.SocketTimeoutException: timeout", these lines will help.
    connectTimeout.set(Duration.ofMinutes(3))
    clientTimeout.set(Duration.ofMinutes(3))
}
