package org.mpierce.guice.warmup;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.Stage;

/**
 * Use various Guice features to get the corresponding classloading, etc, out of the way.
 */
public final class GuiceWarmup {

    /**
     * Instantiate and use a Guice injector.
     *
     * Typically, this takes several hundred ms the first time.
     */
    public static void warmUp() {
        Guice.createInjector(Stage.PRODUCTION, new AbstractModule() {
            @Override
            protected void configure() {
                binder().requireAtInjectOnConstructors();
                binder().requireExactBindingAnnotations();
                binder().requireExplicitBindings();
                binder().disableCircularProxies();

                bind(Depender.class);
            }

            @Provides
            @Singleton
            Dependent getDependent() {
                return new Dependent();
            }
        })
                .getInstance(Depender.class);
    }

    private static class Dependent {}

    private static class Depender {
        @SuppressWarnings({"FieldCanBeLocal", "unused"})
        private final Dependent dependent;

        @Inject
        private Depender(Dependent dependent) {
            this.dependent = dependent;
        }
    }
}
