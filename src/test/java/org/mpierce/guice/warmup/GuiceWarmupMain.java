package org.mpierce.guice.warmup;

final class GuiceWarmupMain {
    public static void main(String[] args) {
        // since this is a timing issue on new VMs, not a correctness issue, we just use a main method

        long start = System.nanoTime();
        GuiceWarmup.warmUp();
        long afterWarmUp = System.nanoTime();

        GuiceWarmup.warmUp();
        long afterSecondWarmUp = System.nanoTime();

        System.out.println(
                "First warmup took " + (afterWarmUp - start) / 1000000 + "ms, second took " + (afterSecondWarmUp - afterWarmUp) / 1000000 + "ms");
    }
}
