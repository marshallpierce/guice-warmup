 [ ![Download](https://api.bintray.com/packages/marshallpierce/maven/guice-warmup/images/download.svg) ](https://bintray.com/marshallpierce/maven/guice-warmup/_latestVersion) 

In your `build.gradle`, add the `jcenter()` repository and:
 
```
implementation("org.mpierce.guice.warmup:guice-warmup:LATEST_VERSION")
```

# What is this?

Guice is a fairly big library, and it takes a good bit of time at JVM startup (hundreds of ms) simply to load the classes needed to use basic Guice features. Fortunately, we can effectively hide that cost by warming up the necessary classes while the rest of startup happens. 

This library has exactly 1 public method that does nothing except exercise enough Guice functionality to pay down much of the startup cost of using Guice.

```
GuiceWarmup.warmUp();
```

# Typical usage

The following shows a straightforward way to use this:

```java
class YourMainClass {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        // Thread pool for startup related tasks
        ExecutorService warmupPool = Executors.newCachedThreadPool();

        // Start GuiceWarmup at the start of main().
        // There's nothing to configure, so do it immediately without waiting for config, etc.
        List<Future<?>> warmupFutures = List.of(
                warmupPool.submit(GuiceWarmup::warmUp)
        );

        // Use warmupPool to parallelize other parts of startup:
        //   - initializing Jackson (slower than you think, especially with extra modules -- many hundreds of ms)
        //     e.g. Future<ObjectMapper> mapperFuture = warmupPool.submit(() -> new ObjectMapper());
        //   - start connection pools for a database, Redis, etc

        // The rest of service startup goes here: read config files, etc.

        // at the end of startup, call get() just to ensure nothing went wrong and threw an exception
        for (Future<?> future : warmupFutures) {
            future.get();
        }

        warmupPool.shutdown();
    }
}
```

Using a structure like this, you should be able to get a JVM from the top of `main()` to listening on a socket in under 1 second, even when using a number of heavy third party libraries.
